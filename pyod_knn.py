from pyod.models.knn import KNN
from data import loadData
from estimate import estimate
from joblib import dump

if __name__ == "__main__":
    x_train, x_test, y_train, y_test = loadData(
        "factory",
        # uncomment for valid only test
        #        skip=[["outlier", "test"]]
    )

    clf = KNN(contamination=0.25)
    clf.fit(x_train)

    prediction = clf.predict(x_test, return_confidence=True)

    print("Testing data:")
    print(x_test)

    print("Labels for test data")
    print(y_test)

    print("Predictions for all test data")
    print(prediction)

    estimate(y_test, prediction[0])
    dump(clf, 'knn.joblib')
