from pyod.models.iforest import IForest
from data import loadData
from estimate import estimate
from joblib import dump

def trainBestModel(x_train, x_test, y_train, y_test, n=50, contamination=0.33):
    bestClf = None
    bestClfRating = None
    for i in range(0, n):
        clf = IForest(contamination=contamination)
        clf.fit(x_train)

        prediction = clf.predict(x_test)
        rating = estimate(y_test, prediction, silent=True)
        print(f"Training model #{i} | Estimation: {rating}")
        if bestClf is None or bestClfRating < rating:
            bestClf = clf
            bestClfRating = rating

    return bestClf


if __name__ == "__main__":
    x_train, x_test, y_train, y_test = loadData("factory")

    clf = trainBestModel(x_train, x_test, y_train, y_test, contamination=0.49)

    #uncomment for valid only test
    #x_train, x_test, y_train, y_test = loadData("factory",
    #    skip=[["outlier", "test"]]
    #)

    prediction = clf.predict(x_test, return_confidence=True)

    print("Testing data:")
    print(x_test)

    print("Labels for test data")
    print(y_test)

    print("Predictions for all test data")
    print(prediction)

    estimate(y_test, prediction[0])
