
def estimate(labels, predictions, silent = False):
    total = len(predictions)
    correct = 0
    for i in range(total):
        if predictions[i] == labels[i]:
            correct += 1

    result = float(correct) / total

    if not silent:
        print(f"Total number of test data: {total}")
        print(f"Correct data: {correct}")
        print(f"Correct {round(result * 100, 2)}%")

    return result
