from pyod.models.iforest import IForest
from data import loadData
from estimate import estimate

if __name__ == "__main__":
    x_train, x_test, y_train, y_test = loadData("factory")

    clf = IForest(contamination=0.33)
    clf.fit(x_train)

    prediction = clf.predict(x_test, return_confidence=True)

    print("Testing data:")
    print(x_test)

    print("Labels for test data")
    print(y_test)

    print("Predictions for all test data")
    print(prediction)

    estimate(y_test, prediction[0])
