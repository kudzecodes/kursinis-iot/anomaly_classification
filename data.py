import glob
import os
import json
import numpy as np


def getListOfFilesToExamine(deviceName):
    files = glob.glob(f"data/{deviceName}/**/*", recursive=True)
    files = filter(lambda x: os.path.isfile(x), files)
    return files


def isTrainData(file):
    return "/train/" in file


def isValidData(file):
    return "/valid/" in file


def parseData(_json):
    data = json.loads(_json)
    return list(map(lambda x: x[0], data.values()))


def parseFile(file):
    f = open(file)
    data = f.read()
    f.close()

    return parseData(data)


def shouldFileBeSkipped(file, skip):
    for row in skip:
        for entry in row:
            if entry not in file:
                return True

    return False


def filterSkipped(files, skip):
    return filter(lambda file: shouldFileBeSkipped(file, skip), files)


def loadData(deviceName, skip=[]):
    files = getListOfFilesToExamine(deviceName)

    if len(skip) != 0:
        files = filterSkipped(files, skip)

    x_train = []
    x_test = []
    y_train = []
    y_test = []

    for file in files:
        if isTrainData(file):
            x_train.append(parseFile(file))
            y_train.append(0 if isValidData(file) else 1)
        else:
            x_test.append(parseFile(file))
            y_test.append(0 if isValidData(file) else 1)

    return np.array(x_train), np.array(x_test), np.array(y_train), np.array(y_test)
